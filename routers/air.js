import moment from 'moment';
import {get_airlines, get_airports, get_flight_search} from '../lib/flightApi';

exports.main = async (ctx, next) => {
    await ctx.render('pages/flight_search',{
        title: 'Search'
    });
};

exports.airlines = async (ctx, next) => {
    let airlines = await get_airlines();
    let content = JSON.parse(airlines);
    await ctx.render('pages/pages',{
        title: 'Airlines',
        content: content
    });

};

exports.airports = async (ctx, next) => {
    let data = ctx.query.q;
    let airlines = await get_airports(data);
    let content = JSON.parse(airlines);
    ctx.body = content;
    ctx.body = airlines;

};

exports.flight_search = async (ctx, next) => {

    let date = ctx.request.body.date;
    let from = ctx.request.body.from;
    let to  = ctx.request.body.to;


    let content = [];
    // get airlines
    let airlines = await get_airlines();
    let listAirlines = JSON.parse(airlines);

    for (let i=0; listAirlines.length > i; i++) {
        let dataForSearch = {
            date: date,
            from: from,
            to: to,
            airline_code: listAirlines[i].code
        };
        let listResulSearch = await get_flight_search(dataForSearch);
        let newcontent = JSON.parse(listResulSearch);
        content.push(newcontent);
    }
    let nextDay1 = moment(date).add(1, 'd').format('YYYY-MM-DD');
    let nextDay2 = moment(date).add(2, 'd').format('YYYY-MM-DD');
    let prevDay1 = moment(date).add(-1, 'd').format('YYYY-MM-DD');
    let prevDay2 = moment(date).add(-2, 'd').format('YYYY-MM-DD');

    let answers = {
        content:content,
        nextDay1: {date: nextDay1, from: from, to: to},
        nextDay2: {date: nextDay2, from: from, to: to},
        prevDay1: {date: prevDay1, from: from, to: to},
        prevDay2: {date: prevDay2, from: from, to: to},
    };
    ctx.body = answers;
};
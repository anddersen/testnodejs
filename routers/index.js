import air from './air';
import Router from 'koa-router';

export function routersMiddleware(options = {}) {
    const router = new Router();

    router.get('/', air.main);
    router.get('/airlines', air.airlines);
    router.get('/airports', air.airports);
    router.post('/flight_search', air.flight_search);

    return router.routes();

}
(function () {

    var formFlightSearcg = {
        selectDatepicker: '#datepicker',
        selectGetAirports: '.get-airports',
        selectFromCode: '#from_code',
        selectToCode: '#to_code',
        loading: ".loading",
        textresults: "#textresults",
        tableresults: "#tableresults",
        changeDate: ".changeDate",
        /**
         Convert airports formats to jQuery UI autocomplete
         */
        airportsValues: function (data, from) {
            var selectFromCode = this.selectFromCode;
            var selectToCode = this.selectToCode;

            var list = [];
            if(data && data.length>0){
                data.forEach(function(airport){
                    list.push(
                        {
                            label: airport.cityName+' '+airport.airportName+' ('+airport.airportCode+')',
                            value: airport.airportName,
                            airportCode: airport.airportCode,
                            cityCode: airport.cityCode
                        }
                    );
                });

                if (from) {
                    $(selectFromCode).val(data[0].airportCode);
                }
                else  {
                    $(selectToCode).val(data[0].airportCode);
                }

            }

            return list;
        },
        flight_search_change_date: function (date, from, to) {
            var data = {
                date: date,
                from: from,
                to: to
            };
            var loading = formFlightSearcg.loading;
            var tableresults = formFlightSearcg.tableresults;
            var textresults = formFlightSearcg.textresults;

            $(loading).addClass('active');
            $(tableresults).html('');
            $.ajax({
                url: '/flight_search',
                type: 'POST',
                data: data,
                success: function(data) {
                    $(loading).removeClass('active');
                    var contentResult = '<table class="table table-hover">';
                    contentResult +='<thead>' +
                        '<tr>' +
                        '<th>Airline name</th>' +
                        '<th>Start</th>' +
                        '<th>Finish</th>' +
                        '<th>Price</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>';
                    for (var i=0;i<data.content.length; i++) {
                        for (var y=0;y<data.content[i].length; y++) {
                            contentResult +='<tr>';
                            contentResult +='<td>'+data.content[i][y].airline.name+'</td>';
                            contentResult +='<td>'+data.content[i][y].start.dateTime+'</td>';
                            contentResult +='<td>'+data.content[i][y].finish.dateTime+'</td>';
                            contentResult +='<td>'+data.content[i][y].price+'</td>';
                            contentResult +='</tr>';
                        }
                    }
                    contentResult +='</tbody></table>';
                    $(tableresults).html(contentResult);
                },
                error: function(data) {
                    $(loading).removeClass('active');
                    $(textresults).html(data.statusText);
                }
            });
            return false;
        },
        flight_search: function () {
            var form = $(this);
            var data = form.serialize();
            var loading = formFlightSearcg.loading;
            var tableresults = formFlightSearcg.tableresults;
            var textresults = formFlightSearcg.textresults;


            $(loading).addClass('active');
            $(tableresults).html('');
            $(textresults).html('');

            $.ajax({
                url: '/flight_search',
                type: 'POST',
                data: data,
                success: function(data) {
                    $(loading).removeClass('active');
                    var contentResultText = '<table class="table text-center"><tbody>';
                    contentResultText +='<tr>';
                    contentResultText +='<td><a class="changeDate" data-from="'+data.prevDay2.from+'" data-to="'+data.prevDay2.to+'" data-date="'+data.prevDay2.date+'">'+data.prevDay2.date+'</a></td>';
                    contentResultText +='<td><a class="changeDate" data-from="'+data.prevDay1.from+'" data-to="'+data.prevDay1.to+'" data-date="'+data.prevDay1.date+'">'+data.prevDay1.date+'</a></td>';
                    contentResultText +='<td><a class="changeDate" data-from="'+data.nextDay1.from+'" data-to="'+data.nextDay1.to+'" data-date="'+data.nextDay1.date+'">'+data.nextDay1.date+'</a></td>';
                    contentResultText +='<td><a class="changeDate" data-from="'+data.nextDay2.from+'" data-to="'+data.nextDay2.to+'" data-date="'+data.nextDay2.date+'">'+data.nextDay2.date+'</a></td>';
                    contentResultText +='</tr>';
                    contentResultText +='</tbody></table>';

                    var contentResult = '<table class="table table-hover">';
                    contentResult +='<thead>' +
                        '<tr>' +
                            '<th>Airline name</th>' +
                            '<th>Start</th>' +
                            '<th>Finish</th>' +
                            '<th>Price</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>';
                    for (var i=0;i<data.content.length; i++) {
                        for (var y=0;y<data.content[i].length; y++) {
                            contentResult +='<tr>';
                            contentResult +='<td>'+data.content[i][y].airline.name+'</td>';
                            contentResult +='<td>'+data.content[i][y].start.dateTime+'</td>';
                            contentResult +='<td>'+data.content[i][y].finish.dateTime+'</td>';
                            contentResult +='<td>'+data.content[i][y].price+'</td>';
                            contentResult +='</tr>';
                        }
                    }
                    contentResult +='</tbody></table>';
                    $(textresults).html(contentResultText);
                    $(tableresults).html(contentResult);
                },
                error: function(data) {
                    $(loading).removeClass('active');
                    $(textresults).html(data.statusText);
                    $(tableresults).html('');
                }
            });
            return false;
        },

        init: function () {
            var selectDatepicker = this.selectDatepicker;
            var selectGetAirports = this.selectGetAirports;
            var changeDate = this.changeDate;
            // hidden input for search
            var selectFromCode = this.selectFromCode;
            var selectToCode = this.selectToCode;
            // method
            var airportsValues = this.airportsValues.bind(this);
            var flight_search = this.flight_search;
            var flight_search_change_date = this.flight_search_change_date;

            // setting city
            var from = true;

            //Init datepicker
            $(selectDatepicker).datepicker({
                dateFormat : "yy-mm-dd"
            });
            //Init autocomplete
            $(selectGetAirports).autocomplete({
                source: function( request, response ) {
                    var elem = this.element;
                    (elem.hasClass("from-city")) ? from = true : from = false;
                    $.ajax({
                        url: '/airports',
                        data: {q: request.term},
                        success: function(data) {
                            response(airportsValues(data, from));
                        }
                    });
                },
                select: function (e, i) {
                    if (from) {
                        $(selectFromCode).val(i.item.airportCode);
                    }
                    else  {
                        $(selectToCode).val(i.item.airportCode);
                    }
                },
                minLength: 2
            });
            //submit search form
            $('#flight_search').submit(flight_search);
            // change date
            $('body').on('click', changeDate,function () {
                flight_search_change_date($(this).data("date"), $(this).data("from"), $(this).data("to"));
            })
        }
    };

    formFlightSearcg.init();

})();
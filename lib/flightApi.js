import request from 'request';

exports.get_airlines = ()=>{
    return new Promise((resolve, reject) => {
        request('http://node.locomote.com/code-task/airlines', (err, response, body) => {
            if (err) {reject(err)}
            resolve(body);
        });
    });
}

exports.get_airports = (data)=>{
    return new Promise((resolve, reject) => {
        request('http://node.locomote.com/code-task/airports?q='+data, (err, response, body) => {
            if (err) {reject(err)}
            resolve(body);
        });
    });
}


exports.get_flight_search = (data)=>{
    return new Promise((resolve, reject) => {
        request('http://node.locomote.com/code-task/flight_search/' +
            data.airline_code+'?date='+data.date+'&from='+data.from+'&to='+data.to+'', (err, response, body) => {
            if (err) {
                reject(err)
            }
            resolve(body);
        });
    });
}
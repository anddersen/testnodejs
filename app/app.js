import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import convert from 'koa-convert';
import path from 'path';
import serve from 'koa-static';
import co from 'co';
import render from 'koa-ejs';

import err from '../middleware/error';
import logger from '../middleware/logger';
import {routersMiddleware} from '../routers';
import config from '../config';

const app = new Koa();


// setting template
render(app, {
    root: path.join(__dirname, '../templates'),
    layout: path.join('layout'),
    viewExt: 'ejs',
    cache: false,
    debug: true
});
// use co for support node < 7
app.context.render = co.wrap(app.context.render);
app.use(convert(bodyParser()));
app.use(logger);
app.use(routersMiddleware());
app.use(serve(path.join(__dirname, '../public')));
app.use(err);

app.listen(config.get('port'), config.get('host') , function () {
    console.log("server listen " + config.get('port'));
});